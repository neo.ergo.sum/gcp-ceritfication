# Networks
> GCP networks are global

## VPC Network types:
- Default
    - Each project has a Default network when deployed
    - One subnet per region
        - No overlapping
    - Default firewall rules
        - Allows ingress RDP/ICMP/SSH form anywhere
        - Allows any ingress traffic from within the default network
- Auto Mode
    - Default network
    - One subnet per region
    - Regional IP allocation
    - Fixed /20 subnets per region
    - Expandable up to /16
- Custom mode
    - No default subnets created
    - Full control of IP ranges
    - Regional IP allocation
    - Expandable to any RFC 1918 size

