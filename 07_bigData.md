# 1. Cloud dataproc
> Managed Hadoop, MapReduce, Spark, Pig and Hive services

- Just request a Hadoop cluster: it will be built in 90 seconds
- Scalable in both ways eneven when jobs are running
- billing is based on 1 second increments with minimum billable increment set to 1 minute
- Billing tip: preemtible VMs can reduce the cost by ~80% 

- once the data is in the cluster, Spark and SparkSQL can be used to perform data-mining: MLlib can be used to run classification algorithms


# 2. Cloud Dataflow
> Strem and batch processing, unified and simplified pipelines

- oriented to senarios where data is not stored somewhere, but instead it is streamed into the system
- clusters are auto-scaled
- no instance provisioning is required: everything is managed
- **ETL** ops can be performed: move, filter, enrich, shape data
- **Data analysis**: batch computation or continuous computation using streaming


### Usecase:
**Source**(BigQuery) >> **Transform**(MapReduce Pipelines defined as needed) >> **Sink**(Cloud storage)
Examples:  
- fraud detection
- financial
- IoT
- analytics
- segmentation analysis in retail


# 3. Big query
> A fully managed data warehouse

> Analytics database; stream data at 100k rows/second

- provides near real-time interactive analysys of massive datasets(hundreds of TBs) using SQL syntax

### Data infusion into BigQuery:
- from cloud storage
- from cloud datastore
- from stream at 100k rows/second rates

### Data usage
- execute super fast SQL against multiple TBs of data
- read/write data into big-query using CloudDataflow, Hadoop and Spark

### Billing:
Two billed components:
- storage
- processing

# 4. Cloud Pub/Sub
> Scalable and flexible enterprise messaging

> Supports many-to-many asynchronous messaging

- Apps make push/pull subsciptions to topics
- Includes support for offline consumers
- At least once delivery at low latency: a small chance exists for some messages to be delivered more than once
- on demand scalability up to 1M messages per second
- works great to interconnect apps in GCP (push/pull between ComputeEngine and AppEngine)

# 5. Cloud Datalab
> Interactive data exploration
- interactive tool for large-scale data exploration, transformation, analysis and virtualization
- integrated, opensource: built on Jupyter(formerly IPython)

