# VM access:
> The creator of a VM has full root privileges

## Lunux: SSH
- SSH from GCP Console or CloudShell via CloudSDK
- SSH from computer or third-party client and generate key-pair
- Requires firewall rule to allow tcp:22

## Windows: RDP
- RDP clients
- Powershell terminal
- Requires setting the Windows password
- Requires firewall rule to allow tcp:3389

# VM Lifecycle
- Provisioning
    - vCPU and Memory allocation
    - Root disk and persisten disk
    - Additional disks
- Staging
    - IP addresses[VPC](internal and external)
    - System image[CloudStorage]
    - Boot
- Running
    - Startup script
    - Access SSH/RDP
    - Modify use
        - get/set metadata
        - Export system image
        - Snapshot persisten disk
        - Move VM to different zone
        - Live migrate
- Stopping
    - Shutdown script
    - Terminated
        - Delete
        - Availability polilcy

# VM state changes from running:
- reset: remains running
- restart: terminated-> running
- reboot (OS: sudo reboot): running-> running
- stop: running-> terminated
- shutdown (OS: sudo shutdown): running-> terminated
- delete: running-> N/A
- preemption: !automatic!


# Availability policy
> Called scheduling options in SDK/API

- Automatic restart:
    - auto VM restart due to crash or maintenance event
    - not preemption or a user-initiated terminate

- On host maintenance:
    - Determines whether host is live-migrated or terminated due to a maintenance event. Live migration is default

- Live migration:
    - During maintenance event, VM is migrated to different hardware without interruption
    - Metadata indicates occurence of live migration


# Terminated VMs:

- Costs:
    - Only disks and reserved IPs are billed
    
- Actions:
    - Change machine type
    - Add or remove attached disks, change auto-delete settings
    - Modify instance tags
    - Modify custom VM or project-wide metadata
    - Remove or set a new static IP
    - Modify VM availability policy
    - Can't change the image of a stopped VM