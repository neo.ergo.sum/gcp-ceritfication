# Pricing:

## Traffic
- Ingress
    - free
- Egress to the same zone(internal IP addresses)
    - free
- Egress to Google products (Youtube, Maps, Drive etc.)
    - free
- Egress to a different GCP service(within same region; exceptions)
    - free
- Egress between zones in the same region
    - $0.01/GB
- Egress to the same zone (external IP address)
    - $0.01/GB
- Egress between regions within the US and Canada
    - $0.01/GB
- Egress between regions, not including traffic between US regions
    - variable by region


## External IP addresses:
- Static IP address (assigned but not used): **$0.01/hour**
- Static/Ephemeral IP address in used on **standard** VM instances (assigned but not used): **$0.004/hour**
- Static and ephemeral IP addresses in use on preemptible VMs: **$0.002/hour**
- Static and ephemeral IP addresses attached to forwarding rules: **free**

**Note:**
GCP Pricing calculator can be used to estimate the costs implied by ComputeEngine + CloudNetwork