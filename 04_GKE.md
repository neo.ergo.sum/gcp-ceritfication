# Of course

# Hybrid cloud
> Anthos is the modern solution for hybrid and multi-cloud systems

- GKE and GKE On-prem create the foundation
- Anthos keeps in sync on-prem and on-cloud microservices
- Anthos offers a righ set of tools such as:
    - Managing services on-premises and in the cloud
    - Monitoring systems and services
    - Migrating apps from VMs into your clusters
    - Maintaining consistent policies across all cluster(on-prem and cloud)



## GCP Marketplace
> Shared repo across on-prem GKE and GKE

## Cloud interconnect
> Links on cloud AnthosServiceMesh with on-prem IstionOpenSource

## Stackdriver
> Shared between on-prem-GKE and GKE. Similar to ELK/Prometheus
- Logging
- Metrics
- Dashboard
- Alerting

## Policy repository
> Usually stored in git on-prem: enforces the policies across on-prem GKE and GKE

