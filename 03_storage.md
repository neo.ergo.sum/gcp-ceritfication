# 1. Cloud storage
> BLOB storage: Unlike file storage, where data is stored in a hierarchy of directories, unlike block storage in which the OS manages the storage as chunks of disk, BLOB storage stores arbitrary bytes that can be accessed with a unique key(most of the times, the unique key is in a form of an URL)

- High performance, internet-scale
- Simple administration
    - Does not require capacity management

- Can be used for a wide variety of needs:
    - Serving content for web-apps
    - Storing data for archival/desaster recovery
    - Distribute big files to end users via direct download

Cloud storage is not a file-system storage. Each entity is identified by an url, that gives the impression that we're working with files, but this is different than the files that we're used to store on our root filesystem of our VMs.

The cloud storage objects are grouped in buckets.  
The cloud storage objects are immutable.  
Data encryption is ensured server-side, before being written to the disk.  
Data encription is ensured in transit be default, hence it happens over https  


# Buckets 
- Attributes
    - Globally unique name
    - Storage class
    - Location (region or multi-region)
    - IAM policies or ACLs
    - Object versioning settings
    - Object lifecycle management rules
- Contents:
    - Files (in a flat namespace)
    - Access control lists

## Access control:
- Google IAM might be enough in most scenarios: policies are inherited by the buckets from the parent object
- ACLs can also be defined for a finer control:
    - Scope: Who has access(user/grou/domain/SA etc)
    - Permission: what level of access (read/write)

## Immutability
- Cloud storage objects are immutable. Changes cannot be performed in place. Instead, new versions ca be created.
- Versioning can be turned on on the bucket on demand: this overrides or deletes all objects in the bucket: you can then list of older version of objects/restore etc.
- If versioning isn't enabled, new object version always overwrites old version

## Lifecycle management policies:
Examples/Usecases:
- delete objects older than
- delete objects created before date
- keep only most recent 3 versions of an objects


# Storage classes
- Multi-regional
    - 99.95%
    - most frequently used
    - high price per GB
    - low price for transfer
    - best fitter for content storage and delivery
- Regional
    - 99.90%
    - most frequently used within a region
    - moderate price per GB
    - low price for transfer
    - best fitter for: in region analytics, transcoding
- Nearline
    - 99%
    - accessed less than once per month
    - low price per GB
    - high price for transfer
    - best fitter for: long tail content, backups
- Coldline
    - 99%
    - accessed less than once per year
    - lowest price per GB
    - highest price for transfer
    - best fitter for: archiving/DR

# Data transfer to cloud storage:
Generic transfer:  
- Online transfer:
    - self-managed via CLI/GUI
- Storage Transfer Service
    - scheduled, managed batch transfers
    - best fitted for TB/PB scale transfers from other clouds or and HTTPS endpoint
- Transfer Appliance
    - rackable appliances to securely ship client's data

Specific transfer:
- BigQuery(import/export tables)
- ComputeEngine (startup scripts, images and general object storage)
- AppEngine (Object storage, logs and Datastore backups)
- Cloud SQL(import/export tables)

# 2. Cloud BigTable storage
> NoSQL home: best performance for document-entities

- high throughput read/write
- offered via same interface as HBase(the opensource alternative)
- native compatibility with big-data, Hadoop ecosystems
- managed(upgrades, scaling, encryption, IAM included) and scalable

## Application API:
Compatible with data-layers, typically used to serve data to apps, dashboards or data services:
- Managed VMs
- HBase REST server
- A Java server running HBase client

## Streming:
Data can be streamed (written event by event) through various frameworks:
- Cloud dataflow streaming
- Spark Streaming
- Storm

## Batch processing:
- Hadoop MapReduce
- Dataflow
- Spark  

# 3. Cloud datastore
> Similar to cloud BigTable, but can scale horizontally

> Managed sharding and replication

> Supports transactions that affect multiple documents at the same time

> Supports SQL-like queries

> Inside GCPs quotas, many operations can be performed for free.


# 4. Cloud SQL
> A managed RDBMS that offers MySQL and PostgresSQLBeta DBaaS

> Supports transactional SQL

Managed SQL includes:
- replication
- failover cross-zones
- scaling (VERTICAL!)
- firewall
- encryption
- accessible from other GCP services 

# 5. Cloud spanner
> Same as Cloud SQL but scalable horizontally up to PB levels

> Managed sharing
