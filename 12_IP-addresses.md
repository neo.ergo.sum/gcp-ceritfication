# VMs IPs
> VMs can have 2 IP addresses(technically this is not true...see external IP section)

- **Internal IP** address:
    - Allocated from subnet range to VMs by DHCP
    - DHCP lease is renewed every 24 hours
    - VM name + IP is registered in a network-scoped DNS

- **External IP** address(optional):
    - VM doesn't know it's external IP; it is mapped to the internal IP(the network stores a lookup table that matches the external IP addr to the internal addr)
    - Lease:
        - Assigned from pool (ephemeral)
        - Reserved (static)
            - reserved IPs that aren't used, are charged at a higher price than those that are in use :)

# DNS resolution
> Each instance has a name that can be resolved to the internal IP address:
- The hostname is the same as the instance name
- FQDN is [hostname].[zone].c.[project-id].internal

> Name resolution is handled by internal DNS resolver:
- Provided as part of the ComputeEngine(169.254.169.254)
- Configured for use on instance via DHCP
- Provides answer for internal and external addresses


# DNS resolution for external addresses
- Instances with external IP addresses can allow connections from hosts outside the project
    - Users connect directly using external IP addresses
    - Admins can also publish DNS records pointing to the instance
        - Public DNS records are NOT published automatically
- DNS records for external addresses can be published using existing DNS servers (outside GCP)
- DNS zones can be hosted using Cloud DNS

# Cloud DNS
- This is Google's DNS service
- Translates domain names into IP addresses
- Low latency
- High Availability (100% SLA)
- Create/Update milions of DNS records
- Managed through UI/CLI/API

# Alias IP ranges
> Allows to assign an alias of IP range to a VM NIC
- This is useful if multiple services are running on a VM, and each service needs to have a different IP address
    - A scenario is when we want to assign IPs to individual containers that run inside the VM: we define an alias range that point to the same VM NIC, then we can use allocate IPs that belong to the alias range to individual containers


