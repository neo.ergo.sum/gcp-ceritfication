# Virtual private cloud networking (VPC)
> Each VPC network is contained in a GCP project<br>

> To interconnect two VPCs across defferent projects, VPC peering can be used

> Shared VPC can also be used, but the control is shifted in the IAM

The defined VPCs have global scope:
- The can have subnets in any region across the globe
- The subnets are streched across all zones inside the region

VPCs have built-in:
- routing tables (no need to create rules to ensure communucation between zones)
- firewall: custom firewall rules can be created, using tags (tag all web-VMs and then apply FW rule to the tag)


# Cloud load balancing
> In an autoscaling enviroment, a global Cloud Load Balancer is deployed in front of the VMs cluster. Users get a single global anycast IP address
- Can provide cross-region LB, including auto multi-region failover
- Traffic goes over google backbone from the closest point of presence to the user
- Backends are selected based on load
- Only healthy backends receive traffic
- No prewarming is required

Google LB types:
|LB type                | Traffic type          | Ports             |
|---                    |---                    |---                |
| Global HTTP(s)        | L7 LB based on load                   | Can route different urls to different backeds |
| Global SSL Proxy      | L4 LB if non-HTTPS SSL based on load  | Supported on specific ports           |
| Global TCP Proxy      | L4 LB if non-SSL TCP                  | Supported on specific ports           |
| Regional              | LB of any traffic(TCP/UDP)            | Supported on any port  |
| Regional internal     | LB of traffic inside a VPC            | Use for internal tiers of multi-tier apps  |


# Cloud DNS
> Create managed zones, then add, edit, delete DNS records

# Cloud CDN
> Uses Google's globally distributed edge caches to cache content close to your users.

> Once the LB is enabled, the CDN can also be enabled using a simple check-box


# Private(on prem/networks in other clouds) to GCP VPC connections:
- VPN:
    - connect on prem-net to VPC networks over IPSEC tunnels
    - external networks and VPC networks can exchange route information via the GCP Cloud Rounter over VPN, using the Border Gateway Protocol

- Direct peering(for people that do not want to use the Internet):
    - Basically adding a dedicated router in the closes GCP point of presence
- Carrier Peering
    - For those that don't have infrastructure to peer directly in a GCP POP, ISP infra can be contracted
    - Important node: this isn't covered by Google SLA
- Dedicated interconnect:
    - One or more direct circuit to GCP POP;
    - If infra topology meets specific standards, they can be covered by Google SLA up to 99.99% Uptime
    - Can be backed-up by VPN for even higher reliability


# Lab:
## Create VM using UI

## Create VM using CLI
```
// Get all zones
gcloud compute zones list 

// Set default compute zone:
gcloud config set compute/zone europe-west3-b

// Get flavors in region
gcloud compute machine-types list | grep europe-west3-b | grep -i micro

// Get images(iamges belong to image-project)
gcloud compute images list | grep -i debi

// Get networks
gcloud compute networks list

// Deploy VM
gcloud compute instances create "my-vm-2" --machine-type "e2-micro" --image-project "debian-cloud" --image "debian-9-stretch-v20200521" --subnet "default"
``` 