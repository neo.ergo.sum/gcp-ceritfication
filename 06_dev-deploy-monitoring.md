# Cloud source repositories
> A GCP-Managed git. Control on GCP source repos can be managed as any resources: through IAM


# Cloud functions (Beta)
> A single purpose function that responds to events without a server or runtime

- functions are usually event-triggered
- javascript functions are then attached to the defined triggers
- that's all :)

# Infrastructure as a Code (Deployment manager)
> Despite imperative commands that can be used to deploy the infrastructure, Infra as a Code is a declarative alternative that can be used to achive the same thing.

- provides repeatable deployments
- .yaml templates can be defined for repeatable deployments
- templates can be changed - Deployment manager consume an updated template to update existing deployments
- templates can be staged and versioned in Cloud source repos

# Stackdriver
- Monitoring
    - platform, system and app metrics
    - uptime/health checks
    - dashboards and alerts
- Logging
    - platform, system and app logs
    - log search, view, filter, export
    - log based metrics
- Debug
    - debug apps
- Error reporting
    - error notifications
    - error dashboard
- Trace
    - latency reporting and sampling
    - per URL latency and reporting
- Profiler (beta)
    - Continuous profiling of CPU and RAM consumption



