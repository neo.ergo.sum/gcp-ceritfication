# Storage
> Performance scales with size, for each catergory below

- Standard
    - up to 64TB for each instance
- SSD
    - up to 64TB for each instance
- Local SSD
    - max 8 x 367GB local SSD partition => 3TB in total


# Network 
**Throughput:**
- Network throughput scales 2 Gbps per CPU (small exceptions)
- Theoretical max of 32Gbps with 16 vCPU or 100Gbps with T4 or V100 GPUs

**Features:**
- Default, auto, custom networks
- Inbound/Outbound firewall rules:
    - IP based
    - Instance/group tags
- Regional HTTPS LB
- Network LB
    - Does not require pre-warming
- Global and multi-regional subnetworks

> A vCPU is equal to 1 hardware hyper-thread


# Machine types:
- Predefined machine types: predefined flavors
    - Standard
    - High memory
    - High CPU
    - Memory-optimized
    - Compute-optimized
    - Shared-core
- Custom machine types:
    - Manual selection of CPUs and RAM

# Pricing
- Per second, with minimum 1 minute
    - vCPUs, GPUs, memory
- Resource based pricing
    - Each vCPU and each GB of memory is billed separately

- Discounts:
    - Sustained use
        - up to 30% discount for VMs that run an entire month
    - Commited use
    - Preemptible VM instances

- Recommendation Engine:
    - Notifies you of underutilized instances
- Free usage limits


# Sole tenant nodes:
- Dedicated host for only your specific project
- Suitable if physical isolation is a must
- Suitable if running on the same host, can bring a segnificant advantage to the VMs
- Bring your own license is possible on SoleTenant nodes.


# Shielded VMs
> For people afraid of boot or kernel level malware
- Secure boot
- Virtual trusted platform module (vTPM)
- Integrity monitoring
- REQUIRES shielded image

