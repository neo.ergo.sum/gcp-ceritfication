# Cloud NAT
> Provides internet access to private instances: VMs that do not have a public IP, can access Internet via Cloud NAT

> Does implement outbound rules. Does NOT allow inbound connections: in other words, instances can initiate a connection for security patching, but external users/services cannot initiate connections to the instance.

> As a general security best practice: assing external IPs only to those VMs that need to be directly exposed


# Private google access
> If turned ON, VMs that have no external IPs, can reach google's APIs and services

> Private google access can be enabled on a subnet by subnet basis

# IAP for TCP forwarding
> IAP TCP forwarding allows you to establish an encrypted tunnel over which you can forward SSH, RDP, and other traffic to VM instances. IAP TCP forwarding also provides you fine-grained control over which users are allowed to establish tunnels and which VM instances users are allowed to connect to.


- applies to all VM instances that you want to be accessible by using IAP.
- allows ingress traffic from the IP range 35.235.240.0/20. This range contains all IP addresses that IAP uses for TCP forwarding.
- allows connections to all ports that you want to be accessible by using IAP TCP forwarding, for example, port 22 for SSH and port 3389 for RDP.

```
gcloud compute ssh vm-internal --zone us-central1-c --tunnel-through-iap
```