> **Machine Learning APIs** enable Apps that see, hear and understand

# TensorFlow
- open source tool to build and run neural network models
    - wide platform support: CPU/GPU, mobile, server, cloud
- fully managed ML service:
    - familiar notebook-based developer experience
    - optimized for Google infrastructure: integrated with BigQuery and Cloud Storage
- pre-trained machine learning models built by Google:
    - Speech: stream reasults in real time. detects 80 languages
    - Vision: identify objects, landmarks, text and content
    - Translate
    - Natural language: structurea and meaning of text

## Usecases:
- structured data:
    - Classification and regression
    - Recommendation
    - Anomaly detection
- unstructured data:
    - Image and video analytics
    - Text analytics  




