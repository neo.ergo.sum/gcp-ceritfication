# 1. Google App Engine Standard Environment
> "Serverless runtimes"

> Spawn in milliseconds

> No SSH access, no FS read/write, no 3rd party binaries

> network access strictly conducted via the AppEngine Services

> On top of free daily quota, pay per insatnce with auto-shutdown

- Advantages:
    - easy to deploy apps
    - autoscale
    - free daily quota
    - usage based pricing

- several SDKs available for dev/test/deploy: devs can do everything they need locally, before movging to the App Engine

- App Engine Standard Env support specific versions of:
    - Java
    - python
    - php
    - go

- Standard App Engine force the app to run sandboxed (no dependency to OS, hardware etc.)
    - no I/O to local files
    - all requests time out at 60 seconds
    - Limits on third-party software



# 2. Google App Engine Flexible Environment
> AppEngines (the runtimes) are managed containers in GKE

> Spawn in minutes

> SSH access(not by default), read/write allowed although ephemeral, 3rd party binaries allowed

> network access 

> Pay for ressources allocation per hour, no automatic shutdown

- Build and deploy containerized apps with a click
- No sandbox constraints
- Can access App Engine resources


# 3. Google Cloud Endpoints and Apigee Edge
> Hide details, enforce contracts

> Provide a simple, versioned interface as a facade in fron of a complex-changeble implementation


- **Cloud Endpoints** helps you create and maintain APIs
    - Pros:
        - Control access and validate calls with JSON Web Tokens and Google API keys
            - Identify web, mobile users with Auth0 and Firebase Authentication
        - Generate client libraries
    - Supported runtime environments:
        - App Engine Flexible Environment
        - Kubernetes Engine
        - Compute Engine
    - Supported clients:
        - Android
        - iOS
        - Javascript

- **Apigee Edge**
> more business oriented: facilitates rate-limiting, quotas, analytics...

> handy to migrate a big monolytic app to cloud in small chunks






