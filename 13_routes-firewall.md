# Cloud routes
> A route is a mapping of an IP range to a destination
Every netowrk has:
- Routes that let instances in a network send traffic directly to each other
- A default route that directs packets to destinations that are outside the network  
**Note:** 
- Firewall rules must also allow the packet
- Default networks have default rules that allow the instances to communicate to each other
- Manually created networks don't


> Routes map traffic to destination networks:
- apply to traffic egressiong a VM
- forward traffic to most specific route
- are created when a subnet is created
- enable VMs in same network to communicate
- destination is in CIDR notation
- traffic is delivered only if it also matches a firewall rule

> Instance routing table:
- A route can apply to one or more instances
- For a route to apply to an instace, the range AND the tags must match
- If no tags are specified, the instance must only belong to the range
- Compute engine creates read-only routing tables for each individual instance


# Firewall rules
> Firewall rules protect the instances from unapproved conenctions both ingress and egress.

- Every VPC network acts as a distributed firewall
- Firewall rules are applied to the network as a whole
- Connections are allowed or denied at the instance level
- Firewall rules are stateful(bi-directional communication is allowed, once a session is established)
- Implied deny all ingress and allow all egress(if all other rules are deleted, these two will still be in place)


> A firewall rule comprises the following parameters:
- Direction:
    - Inbound connections are matched against ingress rules only
    - Outbound connections are matched against egress rules only
- Source or destination: 
    - Ingress direction: sources can be specified as part of the rule with:
        - IP addresses
        - source tags
        - source service account
    - Egress direction: destinations can be specified as part of a rule with
        - one or more IP ranges
- Protocol and port:
    - Any rule can be restricted to apply to: 
        - specific protocol
        - specific combination of ports and protocols
- Action:
     - allow
     - deny
- Priority:
    - Governs the order in which rules are evaluated: first matching rule is applied
- Rule assignment:
    - All rules are assigned to all instances, but you can assign certain rules to certain instances only





